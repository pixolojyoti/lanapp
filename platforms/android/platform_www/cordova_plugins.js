cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
  {
    "id": "cordova-plugin-device.device",
    "file": "plugins/cordova-plugin-device/www/device.js",
    "pluginId": "cordova-plugin-device",
    "clobbers": [
      "device"
    ]
  },
  {
    "id": "cordova-plugin-splashscreen.SplashScreen",
    "file": "plugins/cordova-plugin-splashscreen/www/splashscreen.js",
    "pluginId": "cordova-plugin-splashscreen",
    "clobbers": [
      "navigator.splashscreen"
    ]
  },
  {
    "id": "cordova-plugin-ionic-keyboard.keyboard",
    "file": "plugins/cordova-plugin-ionic-keyboard/www/android/keyboard.js",
    "pluginId": "cordova-plugin-ionic-keyboard",
    "clobbers": [
      "window.Keyboard"
    ]
  },
  {
    "id": "cordova-plugin-hotspot.HotSpotPlugin",
    "file": "plugins/cordova-plugin-hotspot/www/HotSpotPlugin.js",
    "pluginId": "cordova-plugin-hotspot",
    "clobbers": [
      "cordova.plugins.hotspot"
    ]
  },
  {
    "id": "cordova-plugin-websocket-server.WebSocketServer",
    "file": "plugins/cordova-plugin-websocket-server/www/wsserver.js",
    "pluginId": "cordova-plugin-websocket-server",
    "clobbers": [
      "cordova.plugins.wsserver"
    ]
  },
  {
    "id": "cordova-plugin-zeroconf.ZeroConf",
    "file": "plugins/cordova-plugin-zeroconf/www/zeroconf.js",
    "pluginId": "cordova-plugin-zeroconf",
    "clobbers": [
      "cordova.plugins.zeroconf"
    ]
  }
];
module.exports.metadata = 
// TOP OF METADATA
{
  "cordova-plugin-whitelist": "1.3.3",
  "cordova-plugin-device": "2.0.1",
  "cordova-plugin-splashscreen": "5.0.2",
  "cordova-plugin-ionic-webview": "1.2.0",
  "cordova-plugin-ionic-keyboard": "2.0.5",
  "cordova-plugin-hotspot": "1.2.10",
  "cordova-plugin-add-swift-support": "1.7.2",
  "cordova-plugin-websocket-server": "1.4.10",
  "cordova-plugin-zeroconf": "1.3.3"
};
// BOTTOM OF METADATA
});