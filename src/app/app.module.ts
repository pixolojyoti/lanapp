import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { LanPage } from '../pages/lan/lan';
import { ConnectionPage } from '../pages/connection/connection';
import { JoinPage } from '../pages/join/join';

/*IONIC NATIVE PLUGINS*/
import { Hotspot, HotspotNetwork } from '@ionic-native/hotspot';
import { Zeroconf } from '@ionic-native/zeroconf';
import { ConnectionProvider } from '../providers/connection/connection';
import { NetworkInterface } from '@ionic-native/network-interface';
@NgModule({
	declarations: [
		MyApp,
		HomePage,
		LoginPage,
		LanPage,
		ConnectionPage,
		JoinPage
	],
	imports: [
		BrowserModule,
		IonicModule.forRoot(MyApp, {}, {
			links: [
				{ component: HomePage, name: 'Home', segment: 'home' },
				{ component: LoginPage, name: 'Login', segment: 'login' },
				{ component: ConnectionPage, name: 'Connection', segment: 'connection' },
				{ component: JoinPage, name: 'join', segment: 'join' }
			]
		})
	],
	bootstrap: [IonicApp],
	entryComponents: [
		MyApp,
		HomePage,
		LoginPage,
		LanPage,
		ConnectionPage,
		JoinPage
	],

	providers: [
		StatusBar,
		SplashScreen,
		Hotspot,
		Zeroconf,
		{ provide: ErrorHandler, useClass: IonicErrorHandler },
		ConnectionProvider,
		NetworkInterface
	]
})
export class AppModule { }
