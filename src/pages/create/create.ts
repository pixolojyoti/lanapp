import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ConnectionProvider } from '../../providers/connection/connection';

/**
 * Generated class for the CreatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-create',
	templateUrl: 'create.html',
})
export class CreatePage {

	constructor(public navCtrl: NavController, public navParams: NavParams, public connectionProvider: ConnectionProvider) {
	}

	ionViewDidLoad() {


		this.connectionProvider.connectioncreation();

	}

	// 	console.log('ionViewDidLoad CreatePage');
	// 	console.log('chala');
	// 	cordova.plugins.wsserver.stop();
	// 	var ws = cordova.plugins.wsserver.start(0, {
	// 		'onMessage': function(conn, msg) {
	// 			console.log(conn, msg);
	// 		}
	// 	}, function onStart(addr, port) {
	//
	// 		console.log(ws);
	// 		console.log('chala');
	// 		cordova.plugins.zeroconf.register('_http._tcp.', 'local.', 'Jyoti', port, {}, function(result) {
	// 			console.log(result);
	// 			// Here we have successfully advertised the service
	// 		});
	// 	}, function onDidNotStart(reason) {
	// 		console.log('Did not start. Reason: %s', reason);
	// 	});
	// }











}
