import { Component, Input } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ConnectionProvider } from '../../providers/connection/connection';

/**
 * Generated class for the JoinPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-join',
	templateUrl: 'join.html',
})
export class JoinPage {
	hostlist = new Array();




	constructor(public navCtrl: NavController, public navParams: NavParams) {

	}

	ionViewDidLoad() {
		// this.connectionProvider.connectionjoin();
	}



	setHostList(result) {

		this.hostlist.push(result);
	}

	adduser() {
		//this.connectionProvider.establishconnection('jyoti');
	}



}
