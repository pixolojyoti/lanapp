import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LanPage } from './lan';
import { Hotspot, HotspotNetwork } from '@ionic-native/hotspot';

@NgModule({
  declarations: [
    LanPage,
  ],
  imports: [
    IonicPageModule.forChild(LanPage),
    Hotspot
  ],
})
export class LanPageModule {}
