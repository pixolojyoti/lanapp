import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ConnectionProvider } from '../../providers/connection/connection';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
import { LanPage } from "../lan/lan";
import { HomePage } from "../home/home";
import { ConnectionPage } from '../connection/connection';
import { JoinPage } from '../join/join';
@IonicPage()
@Component({
	selector: 'page-login',
	templateUrl: 'login.html',
})
export class LoginPage {
	@ViewChild('userName') uname;
	@ViewChild('password') password;
	constructor(public navCtrl: NavController, public navParams: NavParams, public connectionProvider: ConnectionProvider) {

	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad LoginPage');
	}
	checkLogin() {
		console.log('Hey');
		// console.log(ConnectionProvider.connectedusers);
		this.navCtrl.push(JoinPage);
		// if(this.uname.value.toLowerCase()=="jyoti".toLowerCase() && this.password.value=='pandey'){
		//     this.navCtrl.push(LanPage);
		// }
		// else{
		//   this.navCtrl.push(HomePage);
		//     console.log("%cLogin failed ", "color: red; font-size:12px;");
		//
		// }
	}

}
